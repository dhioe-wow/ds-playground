import os, sys

from google.cloud import bigquery
import numpy as np
import pandas as pd

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"
BQ_PROJECT = 'gcp-wow-rwds-ai-mmm-super-dev'


class Sampler:

    def __init__(
        self, 
        config={},
        project='wx-bq-poc',
        bucket='wx-personal',
        prefix=None
    ):
        self.config = config
        self.bq = bigquery.Client(project=BQ_PROJECT)
        self.project = project
        self.bucket = bucket
        self.prefix = prefix

        self.sample_dates = ', '.join([f"'{dt}'" for dt in self.config['SAMPLE_DATES']])
        self.sample_size = self.config['SAMPLE_SIZE']
        self.sample_table = self.config['SAMPLE_TABLE']
        self.train_split = self.config['TRAIN_SPLIT']

        assert prefix is not None


    def format_sql(self, sql):
        sql = ...
        return sql
        

    def get_sample(self):
        sql = self.format_sql()
        self.bq.query(sql).result()

        sql = f"SELECT * FROM {self.sample_table}"
        df = self.bq.query(sql).result().to_dataframe()
        return df


    def get_partition(self, df, partition_number=10):

        df['train_split'] = np.random.choice(['train', 'test'], len(df), p=[self.train_split, 1-self.train_split])
        df_train = df[df['train_split'] == 'train'].copy()
        df_test = df[df['train_split'] == 'test'].copy()

        dfs =[]
        for train_split, _df in zip(['train', 'test'], [df_train, df_test]):

            gcs_dir = f'gs://{self.bucket}/{self.prefix}/base/{train_split}/'

            _df = _df.sample(frac=1).reset_index(drop=True)
            _dfs = np.array_split(_df, partition_number)

            for i in range(partition_number):
                _dfs[i]['partition_number'] = i
                _dfs[i].sort_values(by=['crn', 'ref_dt'], inplace=True)
                _dfs[i].to_parquet(gcs_dir+f'part_{str(i).zfill(5)}.parquet')
                
            _df = pd.concat(_dfs)
            dfs.append(_df)

        df = pd.concat(dfs)

        df.to_gbq(
            f'{self.sample_table}', 
            project_id=BQ_PROJECT, 
            table_schema=[
                {'name': 'crn', 'type': 'STRING'}, 
                {'name': 'ref_dt', 'type': 'DATE'}, 
                {'name': 'train_split', 'type': 'STRING'},
                {'name': 'partition_number', 'type': 'INT64'}
            ],
            if_exists='replace'
        )
        
        print(f'Base audience - partitioned - are stored in gs://{self.bucket}/{self.prefix}/base/ and `{self.sample_table}`')


    def run(self, partition_number=10):
        df = self.get_sample()
        self.get_partition(df, partition_number)


class CVMSampler(Sampler):

    def __init__(
        self, 
        config={},
        project='wx-bq-poc',
        bucket='wx-personal',
        prefix=None
    ):

        super(CVMSampler, self).__init__(
            config=config,
            project=project,
            bucket=bucket,
            prefix=prefix
        )

    def format_sql(self):
        sql = f"""
            CREATE OR REPLACE TABLE `{self.sample_table}` AS
            SELECT 
                crn, 
                ref_dt
            FROM `gcp-wow-rwds-ai-mmm-super-dev.PROD_MMM.MMM_FINAL_FEATURES_CAMP`
            WHERE ref_dt IN ({self.sample_dates})
            ORDER BY RAND()
            LIMIT {self.sample_size}
        """
        return sql


class InboundDNSSampler(Sampler):

    def __init__(
        self, 
        config={},
        project='wx-bq-poc',
        bucket='wx-personal',
        prefix=None
    ):

        super(InboundDNSSampler, self).__init__(
            config=config,
            project=project,
            bucket=bucket,
            prefix=prefix
        )

    def format_sql(self):
        sql = f"""
            CREATE OR REPLACE TABLE `{self.sample_table}` AS
            SELECT 
                crn, 
                DATE(campaign_start_date) - 15 AS ref_dt
            FROM `gcp-wow-rwds-ai-mmm-super-dev.PROD_MMM.MMM_INBOUND_DATA`
            WHERE DATE(campaign_start_date) - 15 IN ({self.sample_dates})
                AND MODEL IN ('D', 'DR')
            ORDER BY RAND()
            LIMIT {self.sample_size}
        """
        return sql


class InboundSampler(Sampler):

    def __init__(
        self, 
        config={},
        project='wx-bq-poc',
        bucket='wx-personal',
        prefix=None
    ):

        super(InboundSampler, self).__init__(
            config=config,
            project=project,
            bucket=bucket,
            prefix=prefix
        )

    def format_sql(self):
        sql = f"""
            CREATE OR REPLACE TABLE `{self.sample_table}` AS
            SELECT 
                crn, 
                DATE(campaign_start_date) - 15 AS ref_dt
            FROM `gcp-wow-rwds-ai-mmm-super-dev.PROD_MMM.MMM_INBOUND_DATA`
            WHERE DATE(campaign_start_date) - 15 IN ({self.sample_dates})
            ORDER BY RAND()
            LIMIT {self.sample_size}
        """
        return sql

    


if __name__ == '__main__':

    SAMPLE_CONFIG = {
        'SAMPLE_SIZE': 1000, 
        'SAMPLE_DATES': ['2021-06-06', '2021-05-23'],
        'SAMPLE_TABLE': 'gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.DENNY_DS_PLAYGROUND_SAMPLE',
        'TRAIN_SPLIT': 0.7,
    }

    sampler = Sampler(
        SAMPLE_CONFIG,
        project='wx-bq-poc',
        bucket='wx-personal',
        prefix='denny/ds-playground'
    )
    sampler.run()
