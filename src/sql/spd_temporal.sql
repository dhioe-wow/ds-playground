EXPORT DATA OPTIONS(
    uri='{gcs_dir}',
    format='parquet',
    overwrite=true) AS
    
WITH txn_dates AS
(
    SELECT txn_dt
    FROM UNNEST(GENERATE_DATE_ARRAY('{start_date}', '{end_date}', INTERVAL 7 DAY)) AS txn_dt
), 

unique_crns AS
(
    SELECT DISTINCT crn
    FROM `{sample_table}` 
    WHERE partition_number = {partition_number}
        AND train_split = '{train_split}'
),

base AS
(
    SELECT st.crn, st.ref_dt, x.txn_dt
    FROM 
    (
        SELECT crn, txn_dt
        FROM unique_crns CROSS JOIN txn_dates
    ) x 
        JOIN `{sample_table}` st
            ON st.crn = x.crn 
            AND x.txn_dt BETWEEN st.ref_dt + 1 - {window_week} * 7 AND st.ref_dt
            AND st.partition_number = {partition_number}
            AND st.train_split = '{train_split}'
),

offer AS 
(
    SELECT 
        pbma.crn, 
        pbma.campaign_start_date, 
        pbma.offer_type, 
        pbma.Model, 
        CAST(pbma.campaign_week_nbr AS INT) AS campaign_week_nbr,
        pbma.AVG_SPEND_BAND_L, 
        pbma.AVG_SPEND_BAND_H, 
        CAST(pbma.spend_hurdle AS NUMERIC) AS spend_hurdle, 
        CASE WHEN pbma.spend_type ='' THEN NULL ELSE pbma.spend_type END AS spend_type, 
        CAST(pbma.rewards AS NUMERIC) AS rewards, 
        pbma.redeem_flag
    FROM 
        `gcp-wow-rwds-ai-mmm-super-dev.PROD_MMM.MMM_POST_BQ_MASTER_AUDIENCE` pbma
        JOIN unique_crns uc
            ON pbma.crn = uc.crn
    WHERE 
        pbma.offer_type NOT IN ('MP1', 'MP3', 'TM') -- TODO: add this back and handle rewards/spend_hurdle
        AND pbma.post_week_flag = 0
        AND pbma.campaign_start_date BETWEEN '{start_date}' - 42 AND '{end_date}'
),

txn AS 
(
    SELECT  
        uc.crn,
        DATE_TRUNC(ass.start_txn_date + 6, WEEK(SUNDAY)) AS txn_dt, -- get Sunday
        SUM(ass.tot_amt_incld_gst) AS as_weekly_spd
    FROM 
        `wx-bq-poc.loyalty.article_sales_summary` AS ass
        JOIN `wx-bq-poc.loyalty.article_master` AS am
            ON ass.prod_nbr = am.prod_nbr
            AND am.division_nbr = ass.division_nbr 
        JOIN `wx-bq-poc.loyalty.lylty_card_detail` AS lcd
            ON lcd.lylty_card_nbr = ass.lylty_card_nbr
        JOIN unique_crns AS uc
            ON lcd.crn = uc.crn
    WHERE 
        CAST(ass.void_flag AS STRING) != 'Y'
        AND ass.division_nbr IN (1005, 1030)
        AND ass.prod_qty > 0
        AND ass.tot_amt_incld_gst > 0
        AND am.dept_code NOT IN ('00','20','27') --Non-trading, BWS Attached, Cigarettes
        AND am.sgmnt_code NOT IN ('003859804','054534801','203859804','204534801','704534801','9503803810381','003859830') -- Deliveries, Gift cards
        AND (
            am.ctgry_code NOT IN ('2855','2811','0031','1531','2031','7031') -- Front, GC/recharge/tickets, Lotto
            OR am.subcat_code IN ('2855750','2855713') --film, cameras, dissection dump
            OR am.sgmnt_code IN ('285571501','285571503','285571504','285571506','285571507') -- photo processing, accessories
        )
        AND ass.start_txn_date BETWEEN '{start_date}' - 6 AND '{end_date}'
    GROUP BY
        uc.crn,
        DATE_TRUNC(ass.start_txn_date + 6, WEEK(SUNDAY))
),

pre_result AS
(
    SELECT 
        b.crn,
        b.ref_dt,
        b.txn_dt,
        IFNULL(t.as_weekly_spd, 0) AS as_weekly_spd,
        CASE WHEN o.offer_type = '2C' THEN 1 ELSE 0 END AS offer_type_2c,
        CASE WHEN o.offer_type = '4C' THEN 1 ELSE 0 END AS offer_type_4c,
        CASE WHEN o.offer_type = 'MP1' THEN 1 ELSE 0 END AS offer_type_mp1,
        CASE WHEN o.offer_type = 'MP3' THEN 1 ELSE 0 END AS offer_type_mp3,
        CASE WHEN o.offer_type = 'RA' THEN 1 ELSE 0 END AS offer_type_ra,
        CASE WHEN o.offer_type = 'SGU' THEN 1 ELSE 0 END AS offer_type_sgu,
        CASE WHEN o.offer_type = 'SS1' THEN 1 ELSE 0 END AS offer_type_ss1,
        CASE WHEN o.offer_type = 'SS2' THEN 1 ELSE 0 END AS offer_type_ss2,
        CASE WHEN o.offer_type = 'TM' THEN 1 ELSE 0 END AS offer_type_tm,
        CASE WHEN o.offer_type IS NULL THEN 1 ELSE 0 END AS offer_type_none,
        CASE WHEN o.Model = 'M' THEN 1 ELSE 0 END AS model_m, 
        CASE WHEN o.Model = 'R' THEN 1 ELSE 0 END AS model_r, 
        CASE WHEN o.Model IS NULL THEN 1 ELSE 0 END AS model_none,
        IFNULL(o.AVG_SPEND_BAND_L, 0) AS avg_spend_band_l, 
        IFNULL(o.AVG_SPEND_BAND_H, 0) AS avg_spend_band_h,
        IFNULL(o.spend_hurdle, 0) AS spend_hurdle,
        CASE WHEN o.spend_type = 'single' THEN 1 ELSE 0 END AS spend_type_single,
        CASE WHEN o.spend_type = 'multi' THEN 1 ELSE 0 END AS spend_type_multi,
        CASE WHEN o.spend_type IS NULL THEN 1 ELSE 0 END AS spend_type_none,
        IFNULL(o.rewards, 0) AS rewards, 
        IFNULL(o.redeem_flag, 0) AS redeem_flag,
        ROW_NUMBER() OVER(PARTITION BY b.crn, b.ref_dt, b.txn_dt ORDER BY rand()) rn
    FROM base b 
        LEFT JOIN txn t 
            ON b.crn = t.crn 
            AND b.txn_dt = t.txn_dt
        LEFT JOIN offer o 
            ON b.crn = o.crn
            AND o.campaign_start_date + 7 * (o.campaign_week_nbr - 1) + 6 = t.txn_dt
    ORDER BY crn, ref_dt, txn_dt
) 
-- need to dedup; TODO: maybe a better dedup way is needed
SELECT * EXCEPT(txn_dt, rn)
FROM pre_result
WHERE rn = 1;