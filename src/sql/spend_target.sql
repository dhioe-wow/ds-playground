DECLARE start_date DATE;
DECLARE end_date DATE;
SET start_date = (SELECT MIN(ref_dt)+15 FROM `{sample_table}`);
SET end_date = (SELECT MAX(ref_dt)+28 FROM `{sample_table}`);


WITH campaign_table AS (
    SELECT 
        st.crn,
        st.ref_dt,
        ffc.as_wkly_spd_avg
    FROM `{sample_table}` AS st
        JOIN `gcp-wow-rwds-ai-mmm-super-dev.PROD_MMM.MMM_FINAL_FEATURES_CAMP` ffc
            ON st.crn = ffc.crn AND st.ref_dt = ffc.ref_dt
), 

_article_sales_table AS (
    SELECT  
        st.crn,
        st.ref_dt,
        ass.start_txn_date,
        ass.tot_amt_incld_gst
    FROM 
        `wx-bq-poc.loyalty.article_sales_summary` AS ass
        JOIN `wx-bq-poc.loyalty.article_master` AS am
            ON ass.prod_nbr = am.prod_nbr
            AND am.division_nbr = ass.division_nbr 
        JOIN `wx-bq-poc.loyalty.lylty_card_detail` AS lcd
            ON lcd.lylty_card_nbr = ass.lylty_card_nbr
        JOIN `{sample_table}` AS st
            ON lcd.crn = st.crn
    
    WHERE 1=1
        AND ass.start_txn_date BETWEEN start_date AND end_date 
        AND CAST(ass.void_flag AS STRING) != 'Y'
        AND ass.division_nbr IN (1005, 1030)
        AND ass.prod_qty > 0
        AND ass.tot_amt_incld_gst > 0
        AND am.dept_code NOT IN ('00','20','27') --Non-trading, BWS Attached, Cigarettes
        AND am.sgmnt_code NOT IN ('003859804','054534801','203859804','204534801','704534801','9503803810381','003859830') -- Deliveries, Gift cards
        AND (
            am.ctgry_code NOT IN ('2855','2811','0031','1531','2031','7031') -- Front, GC/recharge/tickets, Lotto
            OR am.subcat_code IN ('2855750','2855713') --film, cameras, dissection dump
            OR am.sgmnt_code IN ('285571501','285571503','285571504','285571506','285571507') -- photo processing, accessories
        )
), 

article_sales_table AS (
    SELECT 
        crn,
        ref_dt,
        SUM(tot_amt_incld_gst) / 2.0 AS as_wkly_spd_avg
    FROM _article_sales_table
    WHERE start_txn_date BETWEEN ref_dt + 15 AND ref_dt + 28 -- take 2 weeks average
    GROUP BY crn, ref_dt
)

SELECT 
    st.*,
    IFNULL(ct.as_wkly_spd_avg, IFNULL(ast.as_wkly_spd_avg, 0)) AS target
FROM `{sample_table}` AS st
    LEFT JOIN campaign_table ct
        ON st.crn = ct.crn AND st.ref_dt = ct.ref_dt
    LEFT JOIN article_sales_table ast
        ON ct.crn = ast.crn AND ct.ref_dt = ast.ref_dt;