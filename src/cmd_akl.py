from google.cloud import bigquery
from google.cloud import bigquery_storage
from google.cloud import storage
from datetime import datetime
import shutil

import time
import os, sys
import pandas as pd
import numpy as np

from auto_cmd_akl_utility import AutoCmdAklWrapper
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"


class BaseCMDAKLFeature:
    def __init__(
        self, 
        project='wx-bq-poc', 
        bucket='wx-personal', 
        prefix=None,
        sample_table=None, 
        akl_config=None,
        cmd_config=None
    ):
        
        assert prefix is not None

        self.project = project
        self.bucket = bucket
        self.prefix = prefix
        self.sample_table = sample_table
        self.storage = storage.Client(project=project)
        self.akl_config = akl_config
        self.cmd_config = cmd_config


    def get_cmd_config(self):
        '''Customise your script here'''
        cmd_config = {
            'project': self.project,
            'bucket': self.bucket,
            'prefix': self.prefix,
            'source_file_content': self.cmd_config['source_file_content'],
            'feature_file_content': self.cmd_config['feature_file_content'],
        }
        return cmd_config


    def get_sql_command(self):
        '''Customise your script here'''
        sql_command = ""
        return sql_command


    def get_preprocess_func(self):
        '''Customise your script here'''
        def preprocess_func(df, **kwargs):
            return df
        return preprocess_func

    
    def customize_argo_yaml(argo_dict, args):
        '''Customise your script here'''
        return argo_dict

    
    def customize_conf_yaml(self, conf_dict):
        '''Customise your script here'''
        return conf_dict


    def customize_excel(self, exce_dict):
        '''Customise your script here'''
        return exce_dict

    
    def post_process(self):

        akl_path = f"gs://{self.bucket}/{self.prefix}/akl/output/{self.prefix.replace('_','-').replace('/','-').lower()}/{datetime.today().strftime('%Y-%m-%d')}/feature_selection/"
        _dfs = [
            pd.read_parquet(akl_path + 'feature_selection_output_M.parquet'),
            pd.read_parquet(akl_path + 'feature_selection_output_T.parquet')
        ]
        df = pd.concat(_dfs)
        df.drop(columns=['target', 'cut', 'sample_weight'], inplace=True)
        df.fillna(0, inplace=True)

        df_base = pd.concat([
            pd.read_parquet(f'gs://{self.bucket}/{self.prefix}/base/train/'), 
            pd.read_parquet(f'gs://{self.bucket}/{self.prefix}/base/test/')
        ])

        partition_number = df_base['partition_number'].max() + 1
        for train_split in ['train', 'test']:
            for i in range(partition_number):
                gcs_dir = f'gs://{self.bucket}/{self.prefix}/cmd_final/{train_split}/part_{str(i).zfill(5)}.parquet'
                _df = df_base[(df_base['train_split'] == train_split) & (df_base['partition_number'] == i)]
                _df = _df.merge(df, left_on=['crn', 'ref_dt'], right_on=['crn', 'ref_dt'])
                _df.drop(columns=['train_split', 'partition_number'], inplace=True)
                _df.to_parquet(gcs_dir)

        print("CMD features are stored in gs://wx-personal/denny/ds-playground/cmd_final/")
    

    def run(self):
        
        def _overwrite_permission():
            response = input('[yes/no]: ')
            if response.lower() not in ['yes', 'no']:
                print('Answer only with either yes or no.')
                overwrite_permission()
            else:
                return response
            
        gcs_dir = f'gs://{self.project}/{self.bucket}/{self.prefix}/cmd'
        prompt = f'Data {gcs_dir} already exists. Do you wish to overwrite?'
        
        blobs = self.storage.list_blobs(self.bucket, prefix=f'{self.prefix}')
        blobs_list = [blob for blob in blobs]
        if len(blobs_list) > 0:
            print(prompt)
            response = _overwrite_permission()
            if response == 'no':
                print(f'CMD job has been cancelled.')
                sys.exit(0)
        
        sql_command = self.get_sql_command()
        cmd_config = self.get_cmd_config()
        auto_cmd_akl = AutoCmdAklWrapper(**cmd_config)   
        preprocess_func = self.get_preprocess_func()
        
        auto_cmd_akl.create_cmd_inputs('sql', sql_command, preprocess_func)
        print('='*50)
        auto_cmd_akl.excute_cmd()
        print('='*50)
        
        auto_cmd_akl.detect_end_of_cmd(1)
        print('='*50)

        argo_dict, conf_dict, exce_dict = auto_cmd_akl.generate_akl_config_files()
        conf_dict = self.customize_conf_yaml(conf_dict)
        exce_dict = self.customize_excel(exce_dict)
        auto_cmd_akl.create_akl_inputs(argo_dict, conf_dict, exce_dict)
        auto_cmd_akl.excute_akl()




class CMDAKLFeature(BaseCMDAKLFeature):
    def __init__( 
        self,  
        project='wx-bq-poc', 
        bucket='wx-personal', 
        prefix=None,
        sample_table=None,
        akl_config=None,
        cmd_config=None
    ):

        assert prefix is not None
        if cmd_config is None:
            cmd_config = {
                'source_file_content': [
                    'cmd_b01_01_membership',
                    'cmd_b02_02_txn_dsct',
                    'cmd_b03_03_txn_ctgry',
                    'cmd_b06_04_txn_bskt_scan',
                    'cmd_b07_06_txn_extra',
                    'cmd_b08_07_et_camp',
                    'cmd_b10_08_extra_mem_obit_scan',
                    'cmd_b11_09_txn_subcat',
                    'cmd_b18_13_txn_sup_ts_scan',
                    'cmd_b20_13_txn_time_series_scan',
                    'cmd_b22_17_health',
                    'cmd_b23_18_mem_pref_store',
                    'cmd_b26_21_cust_dist',
                    'cmd_b28_23_et_camp_type',
                    'cmd_b29_24_houshold',
                    'cmd_b30_25_spend_stretch_camp',
                    'cmd_b32_26_txn_dept_scan',
                    'cmd_b33_27_email_open_device',
                    'cmd_b46_39_drivetime'
                ],
                'feature_file_content': None
            }

        super(CMDAKLFeature, self).__init__(
            project=project, 
            bucket=bucket, 
            prefix=prefix,
            sample_table=sample_table, 
            akl_config=akl_config,
            cmd_config=cmd_config
        )


    def get_sql_command(self):
        sql_command = f"""
            SELECT crn, ref_dt, target
            FROM `{self.sample_table}` 
        """
        return sql_command

    
    def customize_conf_yaml(self, conf_dict):
        conf_dict['global']['run_date'] = datetime.today().strftime('%Y-%m-%d')
        conf_dict['global']['modeller'] = self.akl_config['global']['modeller']
        conf_dict['global']['objective'] = self.akl_config['global']['objective']
        conf_dict['global']['target'] = self.akl_config['global']['conf_global_target']
        conf_dict['global']['metric'] = self.akl_config['global']['conf_global_metric']
        conf_dict['feature_selection']['max_cut'] = 0.5
        conf_dict['feature_selection']['output_iteration'] = -1
        return conf_dict


    def customize_excel(self, exce_dict):
        mars_data_dictionary_df = exce_dict['mars_data_dictionary']
        feature_spec_num_df = exce_dict['feature_spec_num']
        feature_spec_cat_df = exce_dict['feature_spec_cat']
        constrain_df = exce_dict['Constrain']
        
        ignored_features = ['target']

        for feature_spec_df in [feature_spec_num_df, feature_spec_cat_df]:
            feature_spec_df.loc[feature_spec_df.feature.str.contains('_flx_'), 'input'] = 'ignore'
            for ignored_feature in ignored_features:
                feature_spec_df.loc[feature_spec_df.feature == ignored_feature, 'input'] = 'ignore'
                    
        return exce_dict




class CVMCMDAKLFeature(BaseCMDAKLFeature):
    def __init__( 
        self, 
        project='wx-bq-poc', 
        bucket='wx-personal', 
        prefix=None,
        sample_table=None, 
        akl_config=None,
        cmd_config=None
    ):

        assert prefix is not None
        if cmd_config is None:
            cmd_config = {
                'source_file_content': [
                    'cmd_b01_01_membership',
                    'cmd_b02_02_txn_dsct',
                    'cmd_b03_03_txn_ctgry',
                    'cmd_b06_04_txn_bskt_scan',
                    'cmd_b07_06_txn_extra',
                    'cmd_b08_07_et_camp',
                    'cmd_b10_08_extra_mem_obit_scan',
                    'cmd_b11_09_txn_subcat',
                    'cmd_b18_13_txn_sup_ts_scan',
                    'cmd_b20_13_txn_time_series_scan',
                    'cmd_b22_17_health',
                    'cmd_b23_18_mem_pref_store',
                    'cmd_b26_21_cust_dist',
                    'cmd_b28_23_et_camp_type',
                    'cmd_b29_24_houshold',
                    'cmd_b30_25_spend_stretch_camp',
                    'cmd_b32_26_txn_dept_scan',
                    'cmd_b33_27_email_open_device',
                    'cmd_b46_39_drivetime'
                ],
                'feature_file_content': None
            }

        super(CMDAKLFeature, self).__init__(
            project=project, 
            bucket=bucket, 
            prefix=prefix,
            sample_table=sample_table, 
            akl_config=akl_config,
            cmd_config=cmd_config
        )


    def get_sql_command(self):
        sql_command = f"""
            SELECT ffc.*, st.target
            FROM `gcp-wow-rwds-ai-mmm-super-dev.PROD_MMM.MMM_FINAL_FEATURES_CAMP` ffc
                JOIN `{self.sample_table}` st
                    ON ffc.crn = st.crn AND ffc.ref_dt = st.ref_dt
        """
        return sql_command


    def get_preprocess_func(self):        
        def preprocess_func(df, **kwargs):
            to_convert = {
                'to_numeric': [
                    'spend_hurdle', 'as_wkly_spd_avg', 'as_wkly_spd_sum', 
                    'txn_tot_amt_4w','txn_tot_dscnt_4w','txn_tot_dscnt_perc_4w','txn_tot_amt_2w','txn_tot_dscnt_2w','txn_tot_dscnt_perc_2w',
                    'avg_wkly_amt_4w','min_wkly_amt_4w','max_wkly_amt_4w','avg_wkly_amt_2w','min_wkly_amt_2w','max_wkly_amt_2w',
                    'l1w_spend_hurdle','l1w_total_rewards','l2w_spend_hurdle','l2w_total_rewards','l3w_spend_hurdle','l3w_total_rewards'
                ],
                'to_datetime': [
                    'ref_dt', 'campaign_start_date', 'partition_date', 'last1_execution_date', 'last2_execution_date', 'last3_execution_date'
                ],
                'to_string': [
                    'woy', 'doy', 'doy_nnh', 'woy_nnh', 'doy_nod', 'woy_nod'
                ]
            }

            for col in to_convert['to_numeric']:
                df[col] = pd.to_numeric(df[col], errors='coerce')
            for col in to_convert['to_datetime']:
                df[col] = pd.to_datetime(df[col], format='%Y-%m-%d', errors='coerce')
            for col in to_convert['to_string']:
                df[col] = df[col].astype(str)
            
            return df

        return preprocess_func


    def customize_conf_yaml(self, conf_dict):
        conf_dict['global']['run_date'] = datetime.today().strftime('%Y-%m-%d')
        conf_dict['global']['modeller'] = self.akl_config['global']['modeller']
        conf_dict['global']['objective'] = self.akl_config['global']['objective']
        conf_dict['global']['target'] = self.akl_config['global']['conf_global_target']
        conf_dict['global']['metric'] = self.akl_config['global']['conf_global_metric']
        conf_dict['feature_selection']['max_cut'] = 0.5
        conf_dict['feature_selection']['output_iteration'] = -1
        return conf_dict


    def customize_excel(self, exce_dict):
        '''Customise your script here'''
        mars_data_dictionary_df = exce_dict['mars_data_dictionary']
        feature_spec_num_df = exce_dict['feature_spec_num']
        feature_spec_cat_df = exce_dict['feature_spec_cat']
        constrain_df = exce_dict['Constrain']

        ignored_features = [ 
            'target', 'redeem_flag', 'as_wkly_spd_avg', 'weekly_inc_sales', 'weekly_spd', 'as_wkly_spd_sum', 
            'campaign_start_date', 'partition_date', 'last1_execution_date', 'last2_execution_date', 'last3_execution_date',
            'campaign_code', 'campaign_length', 'Template_id', 'l1w_campaign_code', 'l2w_campaign_code', 'l3w_campaign_code', 
        ]
    
        for feature_spec_df in [feature_spec_num_df, feature_spec_cat_df]:
            feature_spec_df.loc[feature_spec_df.feature.str.contains('_flx_'), 'input'] = 'ignore'
            for ignored_feature in ignored_features:
                feature_spec_df.loc[feature_spec_df.feature == ignored_feature, 'input'] = 'ignore'
                    
        return exce_dict




if __name__ == '__main__':

    akl_config = {
        'global': {
            'modeller': 'denny',
            'objective': 'regression',
            'conf_global_target': 'target',
            'conf_global_metric': ['rmse'],
        },
        'feature_selection': {
            'max_cut': 0.5
        },
    }
            
    cmd_akl = CMDAKLFeature(
        sample_table='gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.DENNY_DS_PLAYGROUND_SAMPLE',
        akl_config=akl_config,
        project='wx-bq-poc', 
        bucket='wx-personal', 
        prefix='denny/ds-playground',
    )
    
    cmd_akl.run()