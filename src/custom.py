import os, sys
import datetime
from concurrent.futures import ThreadPoolExecutor, as_completed

from google.cloud import storage
from google.cloud import bigquery
import numpy as np
import pandas as pd

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"
BQ_PROJECT = 'gcp-wow-rwds-ai-mmm-super-dev'


class CustomFeature:

    def __init__(
        self,
        project='wx-bq-poc',
        bucket='wx-personal',
        prefix=None,
        sample_table=None
    ):

        self.project = project
        self.bucket = bucket
        self.prefix = prefix
        self.sample_table = sample_table

        self.partition_number = self.get_partitions()


    def format_sql(self, gcs_dir, train_split, p, **kwargs):
        '''Enter your custom sql here. Make sure to order by crn, ref_dt.'''
        sql = ...
        return sql


    def get_partitions(self):
        gcs = storage.Client(project=self.project)
        bucket = gcs.get_bucket(self.bucket)
        blobs = list(bucket.list_blobs(prefix=self.prefix+'/base/train/'))
        return len(blobs)
        
    
    def get_sql_collections(self, **kwargs):
        sql_collections = []
        for train_split in ['train', 'test']:
            for p in range(self.partition_number):
                gcs_dir = f'gs://{self.bucket}/{self.prefix}/temporal_spend/{train_split}/part_{str(p).zfill(5)}_*.parquet'
                sql_collections.append(
                    self.format_sql(gcs_dir, train_split, p)
                )
        
        return sql_collections

    
    def run(self, **kwargs):
        sql_collections = self.get_sql_collections(**kwargs)

        queries = [self.bq.query(sql) for sql in sql_collections]
        executor = ThreadPoolExecutor(5)
        threads = []

        for job in queries:
            threads.append(executor.submit(job.result))

        I = self.partition_number * 2
        for i, future in enumerate(as_completed(threads)):
            future.result()
            print(f'{i+1}/{I} jobs completed', end="\r", flush=True)