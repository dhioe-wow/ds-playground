"""
Distributed socring function
Using Light GBM for model object from auto-ml
"""

import sys
import pyarrow.parquet as pq
import pandas as pd
import os
import pickle
import numpy as np
from auto_ml.preprocessor.numeric import PreprocessorNum
from auto_ml.preprocessor.categorical import PreprocessorCat


def safe_make_folder(i):
    """Makes a folder if not present"""
    try:
        os.mkdir(i)
    except:
        pass


def load_model(path):
    loaded_model = pickle.load(open(path, 'rb'))
    return loaded_model


def get_feature_list(input_path, model_feature_list, loaded_model):
    # get schema
    feature_list = pq.ParquetFile(input_path).metadata.schema.names

    # get spec for both bum and cat
    spec_num = loaded_model.config['spec_num']
    spec_cat = loaded_model.config['spec_cat']

    spec_num_features = spec_num[(spec_num.input != 'ignore') &
                                 (spec_num.new_feature.isin(
                                     model_feature_list))].feature
    spec_cat_features = spec_cat[(spec_cat.input != 'ignore') &
                                 (spec_cat.new_feature.isin(
                                     model_feature_list))].feature

    num_list = spec_num_features[spec_num_features.isin(feature_list)].tolist()
    cat_list = spec_cat_features[spec_cat_features.isin(feature_list)].tolist()

    return num_list, cat_list


def num_convert(col):
    if 'decimal' in str(col.type):
        out = col.to_pandas().astype(np.float32)
    else:
        out = col.cast('float').to_pandas()
    return out


def read_num_df(input_path, num_list):
    _table = pq.read_table(input_path, columns=num_list)
    process_table = lambda x: num_convert(_table.column(x))
    return pd.concat(list(map(process_table, num_list)), axis=1)


def read_cat_df(input_path, cat_list):
    return pd.read_parquet(input_path, columns=cat_list)


def preprocessor(input_path, fea_list, spec, num_cat):
    if num_cat == 'num':
        df = read_num_df(input_path, fea_list)
        _preprocessor = PreprocessorNum

    if num_cat == 'cat':
        df = read_cat_df(input_path, fea_list)
        _preprocessor = PreprocessorCat

    if df.shape[0] != 0:
        pipeline = _preprocessor(spec, update_spec=False)
        processed_data = pipeline.transform(df)
    else:
        processed_data = pd.DataFrame()

    return processed_data


def get_processed_data(input_path, num_list, cat_list, spec_num, spec_cat, ids):
    spec_num = spec_num.reset_index(drop=True)
    spec_cat = spec_cat.reset_index(drop=True)
    processed_num_data = preprocessor(input_path, num_list, spec_num, 'num')
    processed_cat_data = preprocessor(input_path, cat_list, spec_cat, 'cat')

    # get ids
    ids = pd.read_parquet(input_path, columns=ids).reset_index(drop=True)
    output_data = pd.concat([ids,
                             processed_num_data,
                             processed_cat_data], axis=1)
    return output_data


def main():
    loaded_model = load_model("./model_object.pickle")
    input_path = sys.argv[1]

    # get spec for both bum and cat
    spec_num = loaded_model.config['spec_num']
    spec_cat = loaded_model.config['spec_cat']

    # get list of model features
    model_feature_list = loaded_model.columns
    model_feature_list = pd.DataFrame(model_feature_list, dtype=str)[0].apply(
        lambda x: x.split('||')[0])
    num_list, cat_list = get_feature_list(input_path, model_feature_list, loaded_model)
    
    ids = ['crn', 'ref_dt']
    output_data = get_processed_data(input_path, num_list, cat_list, spec_num, spec_cat, ids)
    print("Data Shape is : {}".format(output_data.shape))

    # generate random columns
    output_data['score'] = loaded_model.predict(output_data)
    print(output_data.head(5))

    print("Data Shape post scoring is : {}".format(output_data.shape))
    safe_make_folder('output')
    output_data.to_parquet('./output/processed.parquet')


if __name__ == '__main__':
    main()