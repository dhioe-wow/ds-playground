lightgbm==2.3.0
scikit-learn==0.20.2
fastavro
pandavro
oyaml==0.9
pandas==0.24.1
pyarrow==0.12.0
xlsxwriter==1.1.8
matplotlib==3.0.2