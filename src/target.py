import os, sys

from google.cloud import bigquery
import numpy as np
import pandas as pd

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"
BQ_PROJECT = 'gcp-wow-rwds-ai-mmm-super-dev'


class Target():

    def __init__(
        self, 
        project='wx-bq-poc', 
        bucket='wx-personal', 
        prefix=None,
        sample_table=None, 
        target_type=None
    ):
        self.bq = bigquery.Client(project=BQ_PROJECT)
        self.sample_table = sample_table
        self.project = project
        self.bucket = bucket
        self.prefix = prefix
        self.target_type = target_type

        assert prefix is not None


    def run_sql(self):
        sql = ''.format(
            sample_table=self.sample_table
        )
        df = self.bq.query(sql).result().to_dataframe()

        return df

    def save2bq(self, df):

        df.to_gbq(
            f'{self.sample_table}', 
            project_id=BQ_PROJECT, 
            table_schema=[
                {'name': 'crn', 'type': 'STRING'}, 
                {'name': 'ref_dt', 'type': 'DATE'}, 
                {'name': 'train_split', 'type': 'STRING'},
                {'name': 'partition_number', 'type': 'INT64'},
                {'name': 'target', 'type': self.target_type}
            ],
            if_exists='replace'
        )


    def run(self):

        df = self.run_sql()
        assert 'partition_number' in df.columns
        assert 'train_split' in df.columns
        assert 'target' in df.columns

        self.save2bq(df)

        partition_number = df['partition_number'].max() + 1
        for train_split in ['train', 'test']:
            for i in range(partition_number):
                gcs_dir = f'gs://{self.bucket}/{self.prefix}/target/{train_split}/part_{str(i).zfill(5)}.parquet'
                _df = df[(df['train_split'] == train_split) & (df['partition_number'] == i)]
                _df[['crn', 'ref_dt', 'target']].to_parquet(gcs_dir)

        print(f"Target are stored in gs://{self.bucket}/{self.prefix}/target/ AND `{self.sample_table}_TARGET`")


class SpendTarget(Target):

    def __init__(self, 
        project='wx-bq-poc', 
        bucket='wx-personal', 
        prefix=None,
        sample_table=None, 
        target_type='FLOAT'
    ):
        super(SpendTarget, self).__init__(
            project=project,
            bucket=bucket,
            prefix=prefix,
            sample_table=sample_table,
            target_type=target_type
        )

    
    def run_sql(self):
        with open('src/sql/spend_target.sql', 'r') as f:
            sql = [line for line in f]
            sql = ''.join(sql)

        sql = sql.format(
            sample_table=self.sample_table
        )

        df = self.bq.query(sql).result().to_dataframe()
        return df


if __name__ == '__main__':
    spend_target = SpendTarget(
        sample_table='gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.DENNY_DS_PLAYGROUND_SAMPLE', 
        project='wx-bq-poc', 
        bucket='wx-personal', 
        prefix='denny/ds-playground'
    )
    spend_target.run()