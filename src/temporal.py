import os, sys
import datetime
from concurrent.futures import ThreadPoolExecutor, as_completed

from google.cloud import storage
from google.cloud import bigquery
import numpy as np
import pandas as pd

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"
BQ_PROJECT = 'gcp-wow-rwds-ai-mmm-super-dev'


class TemporalFeature:

    def __init__(
        self,
        project='wx-bq-poc',
        bucket='wx-personal',
        prefix=None,
        sample_table=None,
        window_week=None
    ):

        self.project = project
        self.bucket = bucket
        self.prefix = prefix
        self.sample_table = sample_table
        self.bq = bigquery.Client(project=BQ_PROJECT)
        self.window_week = window_week

        self.start_date, self.end_date = self.get_min_max_date()
        self.partition_number = self.get_partitions()


    def get_min_max_date(self):
        sample_df = pd.concat([
            pd.read_parquet(f'gs://{self.bucket}/{self.prefix}/base/train/'),
            pd.read_parquet(f'gs://{self.bucket}/{self.prefix}/base/test/')
        ])

        min_date = sample_df['ref_dt'].min()
        if self.window_week:
            min_date -= datetime.timedelta(days=self.window_week * 7)
        max_date = sample_df['ref_dt'].max()

        min_date = min_date.strftime("%Y-%m-%d") 
        max_date = max_date.strftime("%Y-%m-%d") 

        return min_date, max_date


    def get_partitions(self):
        gcs = storage.Client(project=self.project)
        bucket = gcs.get_bucket(self.bucket)
        blobs = list(bucket.list_blobs(prefix=self.prefix+'/base/train/'))
        return len(blobs)

    
    def format_sql(self, gcs_dir, sample_table, train_split, p, **kwargs):
        sql = ...
        return sql

    
    def get_sql_collections(self, **kwargs):
        sql_collections = []
        for train_split in ['train', 'test']:
            for p in range(self.partition_number):
                gcs_dir = f'gs://{self.bucket}/{self.prefix}/temporal_spend/{train_split}/part_{str(p).zfill(5)}_*.parquet'
                sql_collections.append(
                    self.format_sql(gcs_dir, train_split, p)
                )
        
        return sql_collections


    def run(self, **kwargs):
        sql_collections = self.get_sql_collections(**kwargs)

        queries = [self.bq.query(sql) for sql in sql_collections]
        executor = ThreadPoolExecutor(5)
        threads = []

        for job in queries:
            threads.append(executor.submit(job.result))

        I = self.partition_number * 2
        for i, future in enumerate(as_completed(threads)):
            future.result()
            print(f'{i+1}/{I} jobs completed', end="\r", flush=True)



class SpendTemporalFeature(TemporalFeature):

    def __init__(
        self,
        project='wx-bq-poc',
        bucket='wx-personal',
        prefix=None,
        sample_table=None,
        window_week=13
    ):

        super(SpendTemporalFeature, self).__init__(
            project=project,
            bucket=bucket,
            prefix=prefix,
            sample_table=sample_table,
            window_week=13
        )


    def format_sql(self, gcs_dir, train_split, p):

        with open('src/sql/spd_temporal.sql', 'r') as f:
            sql = [line for line in f]
            sql = ''.join(sql)

        sql = sql.format(
            gcs_dir=gcs_dir,
            start_date=self.start_date,
            end_date=self.end_date,
            window_week=self.window_week,
            sample_table=self.sample_table,
            train_split=train_split,
            partition_number=p,
        )

        return sql
        



if __name__ == '__main__':
    
    spend_temporal_feature = SpendTemporalFeature(
        sample_table='gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.DENNY_DS_PLAYGROUND_SAMPLE',
        project='wx-bq-poc', 
        bucket='wx-personal', 
        prefix='denny/ds-playground',
        window_week=13
    )

    spend_temporal_feature.run()