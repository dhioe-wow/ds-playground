import shutil
import subprocess
import gspread
import yaml
import pandas as pd
import numpy as np

from google.auth import default as gadefault
from google.cloud import bigquery
from google.cloud import bigquery_storage
from google.cloud import storage
from datetime import datetime

import time
import os

import create_excel_config as ec

gcp_cred = f"/home/jovyan/.config/gcloud/legacy_credentials/"
gcp_cred += f"{os.getenv('JUPYTERHUB_USER')}/adc.json"
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = gcp_cred

#----------------#
# CMD Parameters #
#----------------#
# cmd_input_path #
# cmd_sourc_outp #
# cmd_ouput_path #
#================#

#-----------------#
# AKL Parameters  #
#-----------------#
# cmd_ouput_path  #
# argo.yaml       # 
# config.yaml     #
# excel_base.xlsx #
#=================#

class AutoCmdAklWrapper:
    
    def __init__(self, project, bucket, prefix, source_file_content, feature_file_content):
    
        self.project = project
        self.bucket = bucket
        self.prefix = prefix
        
        # cmd related config
        self.source_file_df = None
        self.source_path_local =  f'local/{prefix}/cmd/source.txt'
        self.source_path_gstor = f'gs://{bucket}/{prefix}/cmd/source.txt'
        self.feature_file_df = None
        self.feature_path_local = f'local/{prefix}/cmd/features.txt'
        self.feature_path_gstor = f'gs://{bucket}/{prefix}/cmd/features.txt'
                
        if source_file_content:
            self.source_file_df = pd.DataFrame(data={'source': source_file_content})
        if feature_file_content:
            self.feature_file_df = pd.DataFrame(data={'features': feature_file_content})
            
        self.base_parque_gstor = f'gs://{bucket}/{prefix}/cmd/base_input.parquet'
        self.cmd_ouput_path = f'gs://{bucket}/{prefix}/cmd/output/'
        
        # akl related config
        self.argo_path_templ = f'src/akl_templates/argo_base.yaml'
        self.conf_path_templ = f'src/akl_templates/config_base.yaml'
        self.exce_path_templ = f'src/akl_templates/excel_base.xlsx'
        self.argo_path_local = f'local/{prefix}/akl/config/argo_base.yaml'
        self.conf_path_local = f'local/{prefix}/akl/config/config_base.yaml'
        self.exce_path_local = f'local/{prefix}/akl/config/excel_base.xlsx'
        self.argo_path_gstor = f'gs://{bucket}/{prefix}/akl/config/argo_base.yaml'
        self.conf_path_gstor = f'gs://{bucket}/{prefix}/akl/config/config_base.yaml'
        self.exce_path_gstor = f'gs://{bucket}/{prefix}/akl/config/excel_base.xlsx'
        
        
    def create_cmd_inputs(self, base_parquet_type, commands, preprocess_func, **kwargs):
        
        df = None
        
        for path_local in [self.source_path_local, self.feature_path_local]:
            try:
                os.remove(path_local)
            except OSError:
                pass
                
        # cmd input config, source.txt
        if self.source_file_df is not None:
            os.makedirs(os.path.dirname(self.source_path_local), exist_ok=True)
            self.source_file_df.to_csv(self.source_path_local, index=False, header=False)
        
        if self.feature_file_df is not None:
            os.makedirs(os.path.dirname(self.feature_path_local), exist_ok=True)
            self.feature_file_df.to_csv(self.feature_path_local, index=False, header=False)

        if base_parquet_type == 'sql':
            print('Step 1/3 Loading Data from BigQuery ....')
            bq = bigquery.Client(project=self.project)
            df = bq.query(commands).result().to_dataframe()
            print('Step 1/3 Loading Data from BigQuery Done')
            print('  Base CMD input parquet shape', df.shape)
        elif base_parquet_type == 'gcs':
            print('gcs is under TODO List')
            # TODO
            '''
            cmd_base_parquet_path_gstor = ''
            cmd_base_parquet_list = []
            while cmd_base_parquet_path_gstor != self.cmd_base_parquet_path or len(cmd_base_parquet_list)==0:
                cmd_base_parquet_path_gstor = input(f'Press Enter {self.cmd_base_parquet_path} to confirm)
                print('Loading BigQuery Exported Files')
            '''
        else:
            raise NameError(base_parquet_type + ' is not available. Please select from {slq, gcs}')

        if df is not None:
            print('Step 2/3 Preprocessing Base Parquet ....')
            df = preprocess_func(df, **kwargs)
            print('Step 2/3 Preprocessing Base Parquet Done')
        else:
            raise ValueError('CMD pivoter input base parquet is None')

        print('Step 3/3 Saving Data to GCP Storage ....')
        df.to_parquet(self.base_parque_gstor, index=False)
        
        storage_client = storage.Client(project=self.project)
        bucket = storage_client.bucket(self.bucket)
        for path_gstor in [self.source_path_gstor, self.feature_path_gstor]:
            gcs_file = path_gstor.replace(f'gs://{self.bucket}/', '')
            try:
                bucket.blob(gcs_file).delete()
            except:
                pass
                        
        if self.source_file_df is not None:
            self.source_file_df.to_csv(self.source_path_gstor, index=False, header=False)
        if self.feature_file_df is not None:
            self.feature_file_df.to_csv(self.feature_path_gstor, index=False, header=False)
        print('Step 3/3 Saving Data to GCP Storage Done')
    
    
    def run_cmd(self):
        input__path = self.base_parque_gstor
        output_path = self.cmd_ouput_path
        
        if self.source_file_df is not None:
            source_path = self.source_path_gstor
        else:
            source_path = 'na'
            
        if self.feature_file_df is not None:
            feature_path = self.feature_path_gstor
        else:
            feature_path = 'na'
            
        bash_string = 'gcloud --project wx-bq-poc dataproc workflow-templates instantiate preprod-spark-cmd2-pivoter-v2 --async --region us-east4 --parameters \\\n'
        bash_string += f'INPUT_PATH={input__path},\\\n'
        bash_string += f'INPUT_TYPE=parquet,\\\n'
        bash_string += f'OUTPUT_PATH={output_path},\\\n'
        bash_string += f'OUTPUT_TYPE=parquet,\\\n'
        bash_string += f'CSV_DELIMITER=\'|\',\\\n'
        bash_string += f'PARTITION_FORMAT=yyyy-MM-dd,\\\n'
        bash_string += f'JOIN_TYPE=left,\\\n'
        bash_string += f'DECIMAL_AS_DOUBLE=true,\\\n'
        bash_string += f'FILE_INCLUDED_SOURCES={source_path},\\\n'
        bash_string += f'FILE_INCLUDED_FEATURES={feature_path},\\\n'
        bash_string += f'FILE_FEATURE_FILTER=na,\\\n'
        bash_string += f'INCLUDE_FUEL=false,\\\n'
        bash_string += f'NO_SUBFOLDER=true,\\\n'
        bash_string += f'OUT_FILE_NUM=50,\\\n'
        bash_string += f'SPARSITY_UPLIMIT=1'
        self.excute_bash(bash_string)
                
                
    def excute_cmd(self):
        
        cmd_partition_list = []
        client = storage.Client(project=self.project)
        for blob in client.list_blobs(self.bucket, prefix=self.cmd_ouput_path.replace(f'gs://{self.bucket}/', '')):
            blob.delete()
        self.run_cmd()
    
    
    def detect_end_of_cmd(self, sleep_interval):
        
        # TODO max_waiting
        # ======================================= #
        # retrieve cmd output partition name list #
        # ======================================= #
        self.cmd_partition_list = []
        while len(self.cmd_partition_list) != 50:
            self.cmd_partition_list = []
            print(f'Checking CMD Pivoter @ {datetime.now().strftime("%d/%m/%Y %H:%M:%S")}')
            client = storage.Client(project=self.project)
            for blob in client.list_blobs(self.bucket, prefix=f'{self.prefix}/cmd/output/'):
                if '.parquet' in blob.name:
                    self.cmd_partition_list.append(blob.name)

            if len(self.cmd_partition_list) == 50:
                print(f'CMD Pivoter Finished @ {datetime.now().strftime("%d/%m/%Y %H:%M:%S")}')
                break
                
            time.sleep(60 * sleep_interval)
        
    
    def generate_akl_config_files(self):
    
        with open(self.argo_path_templ) as file:
            argo_dict = yaml.load(file, Loader=yaml.FullLoader)
        file.close()
        with open(self.conf_path_templ) as file:
            conf_dict = yaml.load(file, Loader=yaml.FullLoader)
        file.close()
    
        argo_dict['spec']['arguments']['parameters'][0]['value'] = self.conf_path_gstor
        argo_dict['metadata']['generateName'] = f'{self.prefix}'.replace('_','-').replace('/','-').lower()
        conf_dict['global']['google_project'] = self.project
        conf_dict['global']['google_bucket'] = self.bucket
        conf_dict['global']['google_subdir'] = f'{self.prefix}/akl/output'
        conf_dict['global']['model_name'] = f'{self.prefix}'.replace('_','-').replace('/','-').lower()
        conf_dict['input']['data_addr'] = self.cmd_ouput_path.replace('gs://','') + '*'
        conf_dict['input']['config_addr'] = self.exce_path_gstor.replace('gs://','')
        
        print('Loading CMD Output Parquet ....')
        cmd_output_parquet_gs_path = f'gs://{self.bucket}/{self.cmd_partition_list[0]}'
        cmd_output_parquet_df = pd.read_parquet(cmd_output_parquet_gs_path)
        print('Loading CMD Output Parquet Done')
        
        cat_threshold = 50
        exce_dict = ec.generate_excel_config(cmd_output_parquet_df, cat_threshold)
        
        return argo_dict, conf_dict, exce_dict
    
    
    def create_akl_inputs(self, argo_dict, conf_dict, exce_dict):
        
        os.makedirs(os.path.dirname(self.argo_path_local), exist_ok=True)
        
        # config.yaml validation
        # validate preprocessing params
        rate = conf_dict['preprocessor']['params']['train_test_holdout']['by_proportion']
        if isinstance(rate, float) or isinstance(rate, int):
            pass
        elif isinstance(rate, list):
            pass
        else:
            raise TypeError("config.yaml ['preprocessor']['params']['train_test_holdout']['by_proportion'] has to be a number or a list")
            
        with open(self.argo_path_local, 'w') as temp_local_file:
            yaml.dump(argo_dict, temp_local_file, default_flow_style=False)
        temp_local_file.close()
        
        with open(self.conf_path_local, 'w') as temp_local_file:
            yaml.dump(conf_dict, temp_local_file, default_flow_style=False)
        temp_local_file.close()
        
        writer = pd.ExcelWriter(self.exce_path_local, engine='xlsxwriter')
        exce_dict['tables'].to_excel(writer, sheet_name='tables', index=False)
        exce_dict['mars_data_dictionary'].to_excel(writer, sheet_name='mars_data_dictionary', index=False)
        exce_dict['feature_spec_num'].to_excel(writer, sheet_name='feature_spec_num', index=False)
        exce_dict['feature_spec_cat'].to_excel(writer, sheet_name='feature_spec_cat', index=False)
        exce_dict['Constrain'].to_excel(writer, sheet_name='Constrain', index=False)
        writer.save()
        
        client = storage.Client(project=self.project)
        bucket = client.get_bucket(self.bucket)
        blob = bucket.blob(self.argo_path_gstor.replace(f'gs://{self.bucket}/',''))
        blob.upload_from_filename(filename=self.argo_path_local)
        
        blob = bucket.blob(self.conf_path_gstor.replace(f'gs://{self.bucket}/',''))
        blob.upload_from_filename(filename=self.conf_path_local)
        
        blob = bucket.blob(self.exce_path_gstor.replace(f'gs://{self.bucket}/',''))
        blob.upload_from_filename(filename=self.exce_path_local)   
        
        
    def excute_akl(self):
        # bash_string = 'gcloud --project wx-bq-poc container clusters get-credentials project-melon --zone=us-west1-a\n'
        # bash_string += f'sudo curl -SSL -o /usr/local/bin/argo https://github.com/argoproj/argo/releases/download/v2.2.0/argo-linux-amd64\n'
        # bash_string += f'sudo chmod +x /usr/local/bin/argo\n'
        # bash_string += f'argo submit {self.argo_path_local}'
        # self.excute_bash(bash_string)
        self.excute_bash(f'/home/jovyan/.local/bin/argo submit {self.argo_path_local}')
    
    
    def excute_bash(self, bash_string):
        print('='*50)
        print('bash input command:'.center(50))
        print('-'*50)
        print(bash_string)
        print('='*50)
        shell_output = subprocess.check_output(bash_string, stderr=subprocess.STDOUT, shell=True, executable='/bin/bash')
        print('bash output results:'.center(50))
        print('-'*50)
        print(shell_output.decode("utf-8") )
        print('='*50)